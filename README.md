# spring-boot-redis-case

#### 介绍
1、维护一个基于jedis的redisUtils工具类
2、基于redisUtils工具类实现各种redis应用实例

#### 软件架构
springBoot2.0.4
mybatisPlus3.0.4
jedis2.9.0


#### 安装教程


1. 这里是列表文本配置application.properties中的redis配置信息
2. 这里是列表文本配置application.ymx中的mysql配置信息
3. 调用com.jyf.redis.common.utils.RedisUtils工具类中的方法即可



#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx


#### 更新日志

1. project init,add RedisUtils,添加利用redis做接口防刷功能的注解
2. 增加猜数字小游戏功能，可单机可网页，欢迎试玩       20201-05-23  


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


