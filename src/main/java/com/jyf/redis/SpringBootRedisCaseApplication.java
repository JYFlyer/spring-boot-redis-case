package com.jyf.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRedisCaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRedisCaseApplication.class, args);
    }

}
