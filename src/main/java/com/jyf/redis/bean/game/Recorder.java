package com.jyf.redis.bean.game;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户操作提示记录
 *
 * @author jyf
 * @version v1.0.0
 * @date 2021/5/22 10:03
 */
@Data
@Builder(toBuilder = true)
public class Recorder implements Serializable {

    private static final long serialVersionUID = 3491941614698280083L;

    /**
     * 次数
     */
    private int times;
    /**
     * 三角形的数量
     */
    private int square;
    /**
     * 圆形的数量
     */
    private int circle;
    /**
     * 数字和位置对的次数，若等于可输入数字的个数，表示胜利
     */
    private int victory;
    /**
     * 记录时间
     */
    private String time;
    /**
     * 用户的输入数字字符串
     */
    private String userNum;
}
