package com.jyf.redis.bean.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


/**
 * 用户实体类
 * @author Mr.贾
 * @time 2020/11/9 20:03
 */
@Data
@TableName("user")
public class User implements Serializable {


    private static final long serialVersionUID = -7766382765722029390L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 头像地址
     */
    private String headImg;
    /**
     * 头像存储地址
     */
    private String headImgDisk;

    /**
     * 出生日期
     */
    private String birthday;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * qq
     */
    private String qq;
    /**
     * 微信
     */
    private String weChat;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 部门ID
     */
    private Long departmentId;

    /**
     * 直属上司ID
     */
    private Long superiorId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 是否在线，0否，1是
     */
    private Integer online;

    /**
     * 创建时间
     */
    private java.util.Date createTime;

    @TableField(exist = false)
    private String departmentName;



}
