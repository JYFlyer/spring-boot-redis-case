package com.jyf.redis.common.annotation;

import java.lang.annotation.*;

/**
 * 接口防刷注解
 * @author jyf
 * @time 2020/10/20 14:33
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccessLimit {

    /**
     * redis过期时间
     */
    int second() default 5;

    /**
     * 过期时间内的最大允许访问次数
     */
    int maxLimit() default 5;


}
