package com.jyf.redis.common.annotation;

import java.lang.annotation.*;

/**
 * 操作日志注解
 * @author jyf
 * @time 2020/10/20 14:33
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    String content() default "";
}
