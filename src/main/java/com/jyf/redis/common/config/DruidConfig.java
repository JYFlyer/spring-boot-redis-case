package com.jyf.redis.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Druid数据源的配置
 * @author jyf
 * @time 2020-09-07 11:02
 */
@Configuration
public class DruidConfig {

    /**
     * 根据配置的属性创建一个新的数据源对象放到IOC容器中
     * @return
     */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource dataSource() {
        return new DruidDataSource();
    }


}
