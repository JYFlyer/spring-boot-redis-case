package com.jyf.redis.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatisPlus的配置
 * @author jyf
 * @time 2020-09-07 11:03
 */
@Configuration
@MapperScan({"com.jyf.redis.dao"})
public class MybatisPlusConfig {

    /**
     * MP内置分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * MP内置sql性能分析插件
     * 用于输出每条 SQL 语句及其执行时间，只在开发和测试环境下使用
     *
     * @return
     */
    //@Bean
    public PerformanceInterceptor performanceInterceptor() {
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        //performanceInterceptor.setFormat(true);//sql格式化输出
        //最大执行时长，超过自动停止执行，单位毫秒
        performanceInterceptor.setMaxTime(2000);
        return performanceInterceptor;
    }


}
