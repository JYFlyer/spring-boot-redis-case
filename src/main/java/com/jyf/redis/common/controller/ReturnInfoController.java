package com.jyf.redis.common.controller;

import com.jyf.redis.common.model.ErrorCodeEnum;
import com.jyf.redis.common.model.Result;
import com.jyf.redis.common.utils.RedisUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Tuple;

import java.util.Set;

/**
 * 接口调用异常时，统一设置返回消息的controller
 *
 * @author Mr.贾
 * @time 2020/10/20 16:10
 */
@RestController
@RequestMapping("/exception/info")
public class ReturnInfoController {
    /**
     * 未登录
     */
    public static final String noLogin = "/exception/info/noLogin";
    /**
     * 用户访问频繁
     */
    public static final String frequentlyAccess = "/exception/info/frequentlyAccess";


    /**
     * 用于返回用户未登录时的提示
     */
    @RequestMapping("/noLogin")
    public Result noLogin() {
        return Result.fail(ErrorCodeEnum.No_Login,ErrorCodeEnum.No_Login.getMessage());
    }

    /**
     * 用于返回用户频繁访问时被限制的提示
     */
    @RequestMapping("/frequentlyAccess")
    public Result frequentlyAccess() {
        return Result.fail(ErrorCodeEnum.FREQUENTLY_ACCESS, "您的手速过快，请稍后再试!");
    }

    //@GetMapping("/test")
    public Result myTest(){
        Set<Tuple> zset = RedisUtils.zrangeWithScores("zset", 0, -1);
        return Result.success("成功",zset);
    }


}
