package com.jyf.redis.common.model;

/**
 * 系统错误码的枚举
 *
 * @author Mr.贾
 * @time 2021/1/2 12:51
 */
public enum ErrorCodeEnum implements ResponseCode{

    No_Login("403","未登录!"),
    FREQUENTLY_ACCESS("405","用户频繁访问!"),
    PARAM_ERROR("444","参数错误!"),
    SERVER_ERROR("500","服务端出错!"),

    //数字游戏相关
    WRONG_ANSWER("10001","回答不正确！"),
    CHALLENGE_FAIL("10002","挑战失败！")


    ;







    private String code;

    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    ErrorCodeEnum(String code, String message){
        this.code = code;
        this.message =message;
    }

    @Override
    public String toString() {
        return "ErrorCodeEnum{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
