package com.jyf.redis.common.model;

/**
 * 响应code的接口
 * @author Mr.贾
 * @time 2021/1/2 13:58
 */
public interface ResponseCode {

    String getCode();

    String getMessage();
}
