package com.jyf.redis.common.model;

import java.io.Serializable;

/**
 * 通用的返回类
 * @author jyf
 * @time 2020/11/9 20:03
 */
public final class Result<T> implements Serializable {

    private static final long serialVersionUID = 5736772872144963354L;
    /**
     * 状态码
     */
    private String code;
    /**
     * 状态码对应的信息
     */
    private String message;
    /**
     * 用户提示信息
     */
    private String tips;
    /**
     * 返回的数据
     */
    private T data;

    /**
     * 处理成功
     */
    public static <T> Result<T> success() {
        return new Result<>(ResultCode.SUCCESS,"处理成功!");
    }
    /**
     * 处理成功
     * @param data 返回的数据
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(ResultCode.SUCCESS, "处理成功!",data);
    }
    /**
     * 处理成功
     * @param tips 用户提示信息
     * @param data 数据
     */
    public static <T> Result<T> success(String tips, T data) {
        return new Result<>(ResultCode.SUCCESS, tips, data);
    }

    /**
     * 处理失败
     * @param tips 用户提示信息
     */
    public static <T> Result<T> fail(String tips) {
        return new Result<>(ResultCode.FAIL,tips);
    }
    /**
     * 处理失败
     * @param tips 用户提示信息
     * @param data 数据
     */
    public static <T> Result<T> fail(String tips, T data) {
        return new Result<>(ResultCode.FAIL,tips,data);
    }
    /**
     * 处理失败
     * @param errorCodeEnum 错误码枚举
     * @param tips 用户提示信息
     */
    public static <T> Result<T> fail(ErrorCodeEnum errorCodeEnum, String tips) {
        return new Result<>(errorCodeEnum,tips);
    }
    /**
     * 处理失败
     * @param errorCodeEnum 错误码枚举
     * @param tips 用户提示信息
     * @param data 数据
     */
    public static <T> Result<T> fail(ErrorCodeEnum errorCodeEnum, String tips, T data){
        return new Result<>(errorCodeEnum,tips,data);
    }

    /**
     * 判断返回结果是否有错误
     * @return true 有错，false没错
     */
    public boolean hasError() {
        return ! this.code.equals(ResultCode.SUCCESS.getCode());
    }

    public void setSuccess(ResultCode resultCode) {
        this.code = resultCode.getCode();
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private Result(ResponseCode responseCode, String tips) {
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
        this.tips = tips;
    }
    private Result(ResponseCode responseCode, String tips, T data) {
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
        this.tips = tips;
        this.data = data;
    }

    private Result() {}

    @Override
    public String toString() {
        return "Result{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", tips='" + tips + '\'' +
                ", data=" + data +
                '}';
    }



}
