package com.jyf.redis.common.model;

/** 响应code的枚举，只有正确的，错误的的错误码放在另一个枚举中
 * @link ErrorCodeEnum
 *
 * @author Mr.贾
 * @time 2020/12/22 10:25
 */
public enum ResultCode implements ResponseCode{

    SUCCESS("200","处理成功!"),
    FAIL("400","处理失败!");


    /**
     * 响应码
     */
    private String code;
    /**
     * 响应码的解释
     */
    private String message;

    ResultCode(String code,String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
