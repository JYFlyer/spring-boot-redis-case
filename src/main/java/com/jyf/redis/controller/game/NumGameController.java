package com.jyf.redis.controller.game;

import com.jyf.redis.bean.game.Recorder;
import com.jyf.redis.common.model.ErrorCodeEnum;
import com.jyf.redis.common.model.Result;
import com.jyf.redis.manager.constant.NumGameConstant;
import com.jyf.redis.manager.utils.TimeUtils;
import com.jyf.redis.service.game.NumGameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;


/**
 * 猜数字游戏 控制器
   


numGame功能清单
1、写接口：1）开始游戏（做一次记录：记录开始时间、用户ID等信息，生成系统数字）
	   2）比对结果(若成功，记录结束时间、同时放到sortedSet中以做排行榜、同时将本条记录持久化到数据库中)


	   游客身份、玩家身份

一开始先后台生成系统数字，然后比对是否成功就行了，之后在完善。
先用session/cookie机制
用户头像、昵称、qq/手机号/微信号等唯一识别


 *
 * @author jyf
 * @time 2021/5/12 22:46
 * @version v1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/api/game/num")
public class NumGameController {

    @Autowired
    private NumGameService numGameService;

    private static final int numCount = 4;
    //本轮可猜次数初始化
    private static final int guessCountLimit = 10;


    /**
     * 开始游戏
     * @param request
     * @return
     */
    @RequestMapping("/start")
    public Result startGame(HttpServletRequest request){
        HttpSession session = request.getSession();
        //最大猜测次数放到session中
        session.setAttribute(NumGameConstant.GUESS_COUNT_LIMIT,guessCountLimit);
        session.setAttribute(NumGameConstant.START_TIME,new Date());
        //或者存到db/缓存中

        //系统数字放到session中
        List<String> sysNum = numGameService.generateSysNm(numCount);
        session.setAttribute(NumGameConstant.SYS_NUM,sysNum);

        //初始化提示记录器
        session.setAttribute(NumGameConstant.REPORTER,new ArrayList<Recorder>(guessCountLimit));

        System.out.println(sysNum);

        Map<String,Object> map = new HashMap<>(2);
        map.put("numCount",numCount);
        map.put("guessCountLimit",guessCountLimit);
        return Result.success(map);
    }

    /**
     *  比对结果
     * @param request
     * @param userNum 用户输入
     * @param time 本次提交的计时时间
     * @return
     */
    @RequestMapping("/compare")
    public Result compare(HttpServletRequest request,String userNum,String time){
        if (StringUtils.isEmpty(userNum)){
           return Result.fail("用户数字为空!");
        }

        Result<List<String>> checkResult = numGameService.checkUserNumStr(numCount, userNum);
        if (checkResult.hasError()){
            return checkResult;
        }
        HttpSession session = request.getSession();
        Object sysNum = session.getAttribute(NumGameConstant.SYS_NUM);
        if (sysNum == null){
           return Result.fail(ErrorCodeEnum.CHALLENGE_FAIL,"本轮已结束,请重新开始!");
        }

        int guessCountLimit = (int)session.getAttribute(NumGameConstant.GUESS_COUNT_LIMIT);
        if (guessCountLimit <= 0){
           return Result.fail(ErrorCodeEnum.CHALLENGE_FAIL,"猜测次数已用完，挑战失败!");
        }
        //比较结果
        List<String> userNumList = checkResult.getData();
        List<String> sysNumList = (List<String>) sysNum;
        Map<String, Integer> map = numGameService.compareNum(sysNumList, userNumList);

        //处理结果
        return numGameService.handleResult(map,session,numCount,userNum,time);
    }

    /**
     * 获取当前用户的记录器的值
     * @param request
     * @return
     */
    @GetMapping("/reporter")
    public Result getReporter(HttpServletRequest request){
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(NumGameConstant.REPORTER);
        List<Recorder> recorderList = new ArrayList<>();
        if (attribute != null){
            recorderList = (List<Recorder>)attribute;
            return Result.success(recorderList);
        }
        return Result.success(recorderList);
    }








}
