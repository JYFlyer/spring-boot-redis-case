package com.jyf.redis.controller.sys;



import com.jyf.redis.bean.sys.User;
import com.jyf.redis.common.model.Result;
import com.jyf.redis.service.sys.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *  用户控制器
 * @author Mr.贾
 * @time 2020/11/9 20:03
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);


    @Autowired
    private UserService userService;


    /**
     * 根据ID查询用户信息
     * @return
     */
    @GetMapping("/detail")
    public Result<User> getCurrentUser(Long userId){
        User currUser = userService.getFullUserById(userId);
        return Result.success(currUser);
    }





}
