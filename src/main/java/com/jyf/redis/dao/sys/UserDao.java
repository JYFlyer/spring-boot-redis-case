package com.jyf.redis.dao.sys;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jyf.redis.bean.sys.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Mr.贾
 * @time 2020/11/9 20:03
 */
@Repository
public interface UserDao extends BaseMapper<User> {


    /**
     * 检查用户名密码是否正确
     * @param username 用户名
     * @param password 密码
     * @return
     */
    User checkLogin(@Param("username") String username, @Param("password") String password);

    /**
     * 获取用户详细信息，包括部门名称
     * @param id
     * @return
     */
    User getFullUserById(Long id);


}
