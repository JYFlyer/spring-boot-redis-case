package com.jyf.redis.manager.constant;

/**
 * 数字游戏常量类
 *
 * @author jyf
 * @time 2021/5/13 21:29
 */
public class NumGameConstant {

    //三角形
    public static final String SQUARE = "square";
    //圆形
    public static final String CIRCLE = "circle";
    //胜利
    public static final String VICTORY = "victory";

    //最大猜测次数
    public static final String GUESS_COUNT_LIMIT = "guessCountLimit";




    //开始时间
    public static final String START_TIME = "startTime";
    //结束时间
    public static final String END_TIME = "endTime";

    // 系统数字
    public static final String SYS_NUM = "sysNum";
    // 用户数字
    public static final String USER_NUM = "userNum";

    //记录器
    public static final String REPORTER = "reporter";

}
