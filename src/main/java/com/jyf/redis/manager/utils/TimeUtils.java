package com.jyf.redis.manager.utils;

import java.util.Date;

/**
 * 功能描述
 *
 * @author jyf
 * @time 2021/5/13 22:08
 */
public class TimeUtils {


    /**
     * 获取两个时间相差的毫秒值
     * @param startTime
     * @param endTime
     * @return
     */
    public static long getDuration(Date startTime, Date endTime){
        return endTime.getTime() - startTime.getTime();
    }
}
