package com.jyf.redis.service.game;

import com.baomidou.mybatisplus.extension.api.R;
import com.jyf.redis.common.model.Result;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * 数字游戏服务接口
 *
 * @author jyf
 * @time 2021/5/13 20:33
 */
public interface NumGameService {

    /**
     * 生成系统数字
     * @param numCount 数字数量
     * @return
     */
    List<String> generateSysNm(int numCount);

    /**
     * 检查用户输入是否合法(是数字、不能重复)
     * @param numCount 数字数量
     * @param userNumStr 用户输入
     * @return 用户数字集合
     */
    Result<List<String>> checkUserNumStr(int numCount,String userNumStr);

    /**
     * 比较，并返回结果
     * @param sysNum 系统数字
     * @param userNum 用户数字
     * @return
     */
    Map<String, Integer> compareNum(List<String> sysNum, List<String> userNum);

    /**
     * 处理比较的结果
     * @param map 比较的结果
     * @param session 用户会话
     * @param numCount 最大数字数量限制
     * @param userNum 用户数字
     * @param time 本次记录时间
     * @return
     */
    Result handleResult(Map<String,Integer> map, HttpSession session,int numCount,String userNum,String time);

}
