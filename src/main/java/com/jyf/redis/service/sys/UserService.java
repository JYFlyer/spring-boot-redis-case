package com.jyf.redis.service.sys;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jyf.redis.bean.sys.User;

import java.util.List;

/**
 * @author Mr.贾
 * @time 2020/11/9 20:03
 */
public interface UserService extends IService<User> {

    /**
     * 逗号分割的字符串转化集合
     * @param ids
     * @return
     */
    List<Long> StringToList(String ids);


    /*检查用户登录名和密码*/
    User checkLoginMessage(String username, String password);

    /**
     * 根据用户id获取用户的详细信息，带部门名称等
     * @param id
     * @return
     */
    User getFullUserById(Long id);


}
