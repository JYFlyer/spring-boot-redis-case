package com.jyf.redis.service.sys.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jyf.redis.bean.sys.User;
import com.jyf.redis.dao.sys.UserDao;
import com.jyf.redis.service.sys.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mr.贾
 * @time 2020/11/9 20:03
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao,User> implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public List<Long> StringToList(String ids) {
        String[] split = ids.split(",");
        List<Long> idList = new ArrayList<>(split.length);
        for (String s : split) {
            idList.add(Long.parseLong(s));
        }
        return idList;
    }


    @Override
    public User checkLoginMessage(String username, String password) {
        return userDao.checkLogin(username,password);
    }


    @Override
    public User getFullUserById(Long id) {
        return userDao.getFullUserById(id);
    }

}
