
//耗时
var duration_second = 0;

var second = 0;
var minute = 0;
var hour = 0;
//计算周的天数
var dayOfWeek = 0;
//计算月的天数
var dayOfMonth = 0;
var week = 0;
var month = 0;
var year = 0;

//定时器标志
var interval = null;

/**
 * 开始计时
 */
function startTimer(){
    duration_second = 0;
    interval = setInterval(compute,1000);
}

/**
 * 计算并显示时间
 */
function compute(){
    duration_second++;

    second++;
    if (second >= 60){
        minute++;
        second = 0;
    }
    if (minute >= 60){
        hour++;
        minute = 0;
    }
    /*if (hour >= 24){
       dayOfWeek++;
       dayOfMonth++;
       hour = 0;
    }
    if (dayOfWeek >= 7){
       week++;
       dayOfWeek = 0;
    }
    //这里应区分平年闰年，以及不同的月份，以当前年份作为标准
    if (dayOfMonth >= 30){
       month++;
       dayOfMonth = 0;
    }
    if (month >= 12){
       year++;
       month = 0;
    }*/

    $("#timer").html(getTakeTime());
}

function getTakeTime() {
    return getFormatMinute()+"分"+getFormatSecond()+"秒";
}

function getFormatMinute() {
    return minute < 10? "0"+minute : minute;
}

function getFormatSecond() {
    return second < 10? "0"+second : second;
}

/**
 * 重置时间
 */
function resetTime(){
    clearInterval(interval);
    duration_second = second = minute = hour = 0;
    dayOfWeek = dayOfMonth = week = month = year = 0;
    $("#timer").html("00分00秒");
}

/**
 * 结束计时
 */
function stop() {
    clearInterval(interval);
    duration_second = second = minute = hour = 0;
    dayOfWeek = dayOfMonth = week = month = year = 0;
}

/**
 * 暂停计时
 * 思路：将当前计时变量保存，下次读取回来继续计时
 */
function pause(){

}
    
