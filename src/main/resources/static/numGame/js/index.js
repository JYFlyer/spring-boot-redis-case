
//服务器域名或ip
var serverHost = "http://localhost:10000";

//用户应该输入的数字数量
var numCount;
//用数组模拟栈结构，保存用户输入和回退操作
var resultStack = [];
//保存数字按钮对应的索引，用于回退操作
var indexStack = [];




loadReporter();


/**
 * 根据后台数据加载记录器的内容
 */
function loadReporter(){
    $.ajax({
        type:"get",
        url:serverHost+"/api/game/num/reporter",
        xhrFields:{
            withCredentials:true //发送请求时带上cookie
        },
        success:function (result) {
            var recorders = result.data;
            for (var i = 0; i < recorders.length; i++) {
                addToReporter(recorders[i]);
            }
        }
    });
}



//模拟开始游戏
$("#startGame").click(function () {

    gameStart();
});

/**
 * 开始游戏
 */
function gameStart(){
    $("#gameLoading").css("display","block");

    //重置时间
    resetTime();
    //重置记录器
    $("#reporter > div:not(:eq(0))").empty();

    //延迟两秒关闭
    setTimeout(function () {
        $("#gameLoading").css("display","none");

        $.ajax({
            type:"post",
            url:serverHost+"/api/game/num/start",
            xhrFields:{
                withCredentials:true //发送请求时带上cookie
            },
            success:function (result) {

                numCount = result.data.numCount;
                $("#guessCountLimit").text(result.data.guessCountLimit);
                //开始计时
                startTimer();

                //按钮可用
                $("#instance .numBtnGroup button").prop("disabled",false);
                $("#optBtn button").prop("disabled",false);
            }
        });
    },2000);
}


/**
 * 数字按钮点击事件
 * 操作：在允许数量范围内，将用户输入放到栈里面，再将栈中的数据同步到页面
 */
$(".numBtnGroup button").click(function () {
    var num = $(this).text();

    var $testToast = $("#testToast");
    $testToast.find(".toast-body").text(num);
    $testToast.toast("show");

    if (resultStack.length >= numCount){
        return;
    }

    //数字压栈和赋值
    resultStack.push(num);
    var result = getArrMembers(resultStack);
    $("#userInput").val(result);

    //元素索引压栈
    //当前按钮在给定选择器下元素集合的索引
    var index = $(this).index("#instance .numBtnGroup button");
    indexStack.push(index);

    //禁用按钮
    $(this).prop("disabled",true);
});


/**
 * 将数组中所有元素拼接成字符串返回
 * @param arr 数组
 * @returns {string}
 */
function getArrMembers(arr){
    var result ="";
    for (var i = 0; i < arr.length; i++) {
        result+=arr[i];
    }
    return result;
}

/**
 * 回退按钮点击事件
 * 操作：将末尾数字删除，并将按钮启用
 */
$("#optBtn button:eq(0)").click(function () {
    if (resultStack.length < 1){
        return;
    }
    resultStack.pop();//数字栈顶弹出
    var result = getArrMembers(resultStack);
    $("#userInput").val(result);

    //给定索引位置的btn启用
    var index = indexStack.pop();
    $("#instance .numBtnGroup button:eq("+index+")").prop("disabled",false)
});

/**
 * 提交按钮点击事件
 */
$("#optBtn button:eq(1)").click(function () {
    if (resultStack.length < numCount){
        alert("必须为"+numCount+"个数字才行!");
        return;
    }

    var userNum = $("#userInput").val();
    var time = getFormatMinute()+":"+getFormatSecond();
    $.ajax({
        type:"post",
        url: serverHost+"/api/game/num/compare",
        data:{userNum:userNum,time:time},
        beforeSend:function () {
            
        },
        xhrFields:{
            withCredentials:true
        },
        success:function (result) {
            console.log(result);

             refreshBtnGroup();


            if (result.code == "200"){
                $("#guessCountLimit").text(result.data.guessCountLimit);
                gameEnd();
                successLayer(result.tips+"耗时："+getTakeTime());
                addToReporter(result.data.recorder);
            }else {
                errorHandle(result);
            }
        }

    });
});

/**
 * 重置按钮和输入框
 */
function refreshBtnGroup() {
    for (var i = 0; i < indexStack.length; i++) {
        $("#instance .numBtnGroup button:eq("+indexStack[i]+")").prop("disabled",false);
    }
    $("#userInput").val("");

    indexStack = [];
    resultStack = [];
}


/**
 * 统一处理后台错误返回
 * @param result
 */
function errorHandle(result) {
    switch (result.code){
        case "400":
            toastShow(result.tips);
            break;
        case "405":
            toastShow(result.tips);
            break;
        case "10001":
            $("#guessCountLimit").text(result.data.guessCountLimit);
            toastShow(result.tips);
            addToReporter(result.data.recorder);
            break;
        case "10002":
            gameEnd();

            layer.open({
                title: '结果',
                content: result.tips,
                closeBtn: 0,
                shade: [0.8, '#393D49'],
                resize:false,
                area:['350px', '200px'],
                btn: ['我再看看','重新开始'],
                btn1:function(index, layero){
                    layer.msg('好大好大', {icon: 1});
                },
                btn2:function(index){
                    gameStart();
                }
            });

        default:
            toastShow("未知异常");
    }
    
}

/**
 * toast 显示
 * @param tips 提示
 */
function toastShow(tips) {
    var $toast = $("#resultToast .toast");
    $toast.find(".toast-body").text(tips);
    $toast.toast("show");
}

/**
 * 成功的弹窗
 * @param tips
 */
function successLayer(tips) {
    layer.confirm(tips, {
        btn: ['我在看看','再来一局']
    }, function(){
        layer.msg('好大好大', {icon: 1});
    }, function(){
        gameStart();
    });
}

/**
 * 记录三角形、圆形、时间等提示
 * @param recorder 记录
 */
function addToReporter(recorder){
    var $reporter = $("#reporter");

    var $row = $("<div class='row'></div>");
    var $times = $("<div class='col-2 text-center'>"+recorder.times+"</div>");
    var $square = $("<div class='col-2 text-center'>"+recorder.square+"</div>");
    var $circle = $("<div class='col-2 text-center'>"+recorder.circle+"</div>");
    var $userNum = $("<div class='col-3 text-center'>"+recorder.userNum+"</div>");
    var $time = $("<div class='col-3 text-center'>"+recorder.time+"</div>");
    $row.append($times).append($square).append($circle).append($userNum).append($time).appendTo($reporter);
}

/**
 * 游戏结束时的处理
 */
function gameEnd() {
    //关闭计时
    clearInterval(interval);
    $("#instance .numBtnGroup button").prop("disabled",true);
    $("#optBtn button").prop("disabled",true);
}


