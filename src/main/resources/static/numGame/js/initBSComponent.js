
//初始化 tooltip 组件，使其工作。
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
});

//初始化所有 toast 组件，使其工作。
var toastElList = [].slice.call(document.querySelectorAll('.toast'));
var toastList = toastElList.map(function (toastEl) {
    //初始化设置
    return new bootstrap.Toast(toastEl, {
        delay:3000
    });
});
