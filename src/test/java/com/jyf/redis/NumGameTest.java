package com.jyf.redis;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 单机版 数字游戏
 * 玩法：
 *   系统随机从0-9当中抽取【4】个数字，然后用户从0-9当中选择4个数字，如果用户猜的数字和系统生成的完全一样，则表示猜对了。
 * 规则：
 *   如果猜中的数字对了，就加圆，对一个数字加一个圆；
 *   如果猜中的数字和数字的位置都对，加三角形，数字和数字的位置都对一个，加一个三角形。
 *   若数字和数字的位置全对了，victory!!!
 * 提示：根据每次得到的圆形和三角形的数量，推断出本次猜中了那些数字和位置，最终一步步逼近正确答案！
 * 额外规则：
 *   猜的次数限制。
 *   时间限制。
 *
 * @author jyf
 * @time 2021/5/13 21:10
 */
public class NumGameTest {


    private static final int numCount = 4;
    private static final boolean enableRepeat = false;
    private static final int guessCountLimit = 10;

    //三角形
    private static final String square = "square";
    //圆形
    private static final String circle = "circle";
    //胜利
    private static final String victory = "victory";


    private static final String numRegExp = "\\d{4}";
    private static final Pattern pattern = Pattern.compile(numRegExp);

    public static void main(String[] args) {
        List<String> sysNum = generateSysNm(numCount);
        System.out.println("系统生成的数字为："+sysNum);
        Scanner s = new Scanner(System.in);
        int guessCount = 1;
        while (guessCount <= guessCountLimit){
            System.out.println("请用户输入数字：");
            String userNumStr = s.next();

            List<String> userNum = checkUserNumStr(userNumStr);

            //处理用户输入
            Map<String, Integer> resultMap = compareNum(sysNum, userNum);
            //输出结果
            if (resultMap.get(victory).equals(numCount)){
                System.out.println("恭喜你猜对了！答案就是："+sysNum);
                System.exit(0);
            }else {
                System.out.println("三角形数量："+resultMap.get(square));
                System.out.println("圆形数量："+resultMap.get(circle));

                guessCount++;
            }
        }

        System.out.println("啊哦、次数用完了！");

    }


    private static List<String> generateSysNm(int numCount) {
        String original = "0123456789";
        Set<String> sysNum = new HashSet<>(numCount);
        for (;;) {
            char c = original.charAt(new Random().nextInt(original.length()));
            sysNum.add(String.valueOf(c));
            if (sysNum.size() >= numCount){
                return new ArrayList<>(sysNum);
            }
        }
    }

    private static List<String> checkUserNumStr(String userNumStr){
        Matcher matcher = pattern.matcher(userNumStr);
        if (!matcher.matches()){
            throw new IllegalArgumentException("必须为数字!");
        }
        //查重
        List<String> userNum = new ArrayList<>(numCount);
        for (int i = 0; i < numCount; i++) {
            userNum.add(String.valueOf(userNumStr.charAt(i)));
        }
        Set<String> set = new HashSet<>(userNum);
        if (set.size() < userNum.size()){
            throw new IllegalArgumentException("不能有重复数字!");
        }
        return userNum;
    }

    public static Map<String, Integer> compareNum(List<String> sysNum, List<String> userNum) {
        Map<String,Integer> resultMap = new HashMap<>();
        resultMap.put(square,0);
        resultMap.put(circle,0);
        resultMap.put(victory,0);
        int k = 0;
        for (int i = 0; i < userNum.size(); i++) {
            for (int j = 0; j < sysNum.size(); j++) {
                if (userNum.get(i).equals(sysNum.get(i))){
                    resultMap.put(square,resultMap.get(square) + 1);
                    resultMap.put(victory,resultMap.get(victory) + 1);
                }else {
                    if (sysNum.contains(userNum.get(i))){
                        resultMap.put(circle,resultMap.get(circle) + 1);
                    }
                }
                break;
            }
        }

        return resultMap;
    }



}
